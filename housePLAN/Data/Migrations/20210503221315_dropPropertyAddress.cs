﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace housePLAN.Data.Migrations
{
    public partial class dropPropertyAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PropertyAddresses");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PropertyAddresses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BuildDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    County = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    District = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsFreehold = table.Column<bool>(type: "bit", nullable: false),
                    NameOrNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NewBuild = table.Column<bool>(type: "bit", nullable: false),
                    Postcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PropertyType = table.Column<int>(type: "int", nullable: false),
                    Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TownOrCity = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyAddresses", x => x.Id);
                });
        }
    }
}
