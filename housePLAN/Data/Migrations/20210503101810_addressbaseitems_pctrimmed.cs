﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace housePLAN.Data.Migrations
{
    public partial class addressbaseitems_pctrimmed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PostcodeTrimmed",
                table: "AddressBaseItems",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PostcodeTrimmed",
                table: "AddressBaseItems");
        }
    }
}
