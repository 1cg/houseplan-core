﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace housePLAN.Data.Migrations
{
    public partial class AddressBaseItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AspNetUserTokens",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserTokens",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "ProviderKey",
                table: "AspNetUserLogins",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserLogins",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.CreateTable(
                name: "AddressBaseItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UPRN = table.Column<double>(type: "float", nullable: false),
                    ORGANISATION_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BUILDING_NUMBER = table.Column<int>(type: "int", nullable: true),
                    BUILDING_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SUB_BUILDING_NAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DEPENDENT_THOROUGHFARE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    THOROUGHFARE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    POST_TOWN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DEPENDENT_LOCALITY = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    POSTCODE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    X_COORDINATE = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Y_COORDINATE = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    LATITUDE = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    LONGITUDE = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddressBaseItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AddressBaseItems");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AspNetUserTokens",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserTokens",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "ProviderKey",
                table: "AspNetUserLogins",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserLogins",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");
        }
    }
}
