﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace housePLAN.Data.Migrations
{
    public partial class fileattachment_fleshout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "FileAttachments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "FileAttachments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Guid",
                table: "FileAttachments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "FileAttachments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UploadedOn",
                table: "FileAttachments",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "FileAttachments");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "FileAttachments");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "FileAttachments");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "FileAttachments");

            migrationBuilder.DropColumn(
                name: "UploadedOn",
                table: "FileAttachments");
        }
    }
}
