﻿using housePLAN.Data;
using housePLAN.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace housePLAN.Controllers
{
    public class SearchController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SearchController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PropertyAddresses
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Results(Models.VM.SearchPropertyVM model)
        {
            if (string.IsNullOrEmpty(model.Postcode) && string.IsNullOrEmpty(model.Street))
            {
                return Redirect(nameof(Index));
            }

            var abdata = _context.AddressBaseItems.AsQueryable();
            if (!string.IsNullOrEmpty(model.Postcode))
            {
                string pc = model.Postcode.Replace(" ", "").ToUpper();
                abdata = abdata.Where(i => i.PostcodeTrimmed == pc);
            }
            if (!string.IsNullOrEmpty(model.Street))
            {
                abdata = abdata.Where(i => i.DEPENDENT_THOROUGHFARE.Contains(model.Street));
            }

            var abdataResults = await abdata.Take(100).OrderBy(i=>i.BUILDING_NUMBER).ToListAsync();
            var data = new List<housePLAN.Models.VM.PropertyAddressVM>();

            var fa = await _context.FileAttachments.Where(i => abdata.Select(s => s.Id).Contains(i.AddressBaseItemId)).Select(s => s.AddressBaseItemId).Distinct().ToListAsync();

            foreach (var item in abdataResults)
            {
                data.Add(new Models.VM.PropertyAddressVM()
                {
                    Address = item,
                    HasFiles = fa.Where(i=>i == item.Id).Any()
                });
            }
            return View(data);
        }
    }
}
