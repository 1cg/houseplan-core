﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using housePLAN.Data;
using housePLAN.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace housePLAN.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FileAttachmentsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FileAttachmentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: FileAttachments
        public async Task<IActionResult> Index()
        {
            return View(await _context.FileAttachments.ToListAsync());
        }

        // GET: FileAttachments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fileAttachment = await _context.FileAttachments
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fileAttachment == null)
            {
                return NotFound();
            }

            return View(fileAttachment);
        }

        // GET: FileAttachments/Create
        public IActionResult Create(int? id) // addressbaseid
        {
            if (!id.HasValue)
            {
                // send them to find the property first...
                return View(nameof(FindProperty));
            }

            ViewData["AbId"] = id;
            return View();
        }

        public IActionResult FindProperty()
        {
            return View();
        }
        public async Task<IActionResult> Results(Models.VM.SearchPropertyVM model)
        {
            if (string.IsNullOrEmpty(model.Postcode) && string.IsNullOrEmpty(model.Street))
            {
                return null;
            }

            var abdata = _context.AddressBaseItems.AsQueryable();
            if (!string.IsNullOrEmpty(model.Postcode))
            {
                string pc = model.Postcode.Replace(" ", "").ToUpper();
                abdata = abdata.Where(i => i.PostcodeTrimmed == pc);
            }
            if (!string.IsNullOrEmpty(model.Street))
            {
                abdata = abdata.Where(i => i.DEPENDENT_THOROUGHFARE.Contains(model.Street));
            }

            var abdataResults = await abdata.Take(100).OrderBy(i => i.BUILDING_NUMBER).ToListAsync();
            var data = new List<housePLAN.Models.VM.PropertyAddressVM>();

            foreach (var item in abdataResults)
            {
                data.Add(new Models.VM.PropertyAddressVM()
                {
                    Address = item
                });
            }
            return PartialView(data);
        }

        // POST: FileAttachments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AddressBaseItemId,Description,Guid,UploadedOn,FileName,Password,Id,CreatedOn")] FileAttachment fileAttachment)
        {
            fileAttachment.Id = 0;
            if (ModelState.IsValid)
            {
                fileAttachment.Guid = Guid.NewGuid().ToString();
                _context.Add(fileAttachment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Upload), new { id = fileAttachment.Id });
            }
            return View(fileAttachment);
        }

        // GET: FileAttachments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fileAttachment = await _context.FileAttachments.FindAsync(id);
            if (fileAttachment == null)
            {
                return NotFound();
            }
            return View(fileAttachment);
        }

        // POST: FileAttachments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AddressBaseItemId,Description,Guid,UploadedOn,FileName,Password,Id,CreatedOn")] FileAttachment fileAttachment)
        {
            if (id != fileAttachment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fileAttachment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FileAttachmentExists(fileAttachment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(fileAttachment);
        }

        // GET: FileAttachments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fileAttachment = await _context.FileAttachments
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fileAttachment == null)
            {
                return NotFound();
            }

            return View(fileAttachment);
        }

        // POST: FileAttachments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fileAttachment = await _context.FileAttachments.FindAsync(id);
            _context.FileAttachments.Remove(fileAttachment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FileAttachmentExists(int id)
        {
            return _context.FileAttachments.Any(e => e.Id == id);
        }





        public async Task<IActionResult> Upload(int id)
        {
            var doc = await _context.FileAttachments.Where(i => i.Id == id).FirstOrDefaultAsync();
            if (doc == null)
            {
                return NotFound();
            }
            return View(doc);
        }


        [HttpPost]
        public async Task<ActionResult> Upload(FileAttachment doc, IFormFile file)
        {

            var requieddoc = await _context.FileAttachments.Where(i => i.Id == doc.Id).FirstOrDefaultAsync();
            if (file != null && requieddoc != null)
            {
                using (var stream = new FileStream($"uploadeddocs/{requieddoc.Guid}-{file.FileName}", FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                requieddoc.Uploaded = true;
                requieddoc.UploadedOn = DateTime.Now;
                requieddoc.FileName = $"{requieddoc.Guid}-{file.FileName}";

                await _context.SaveChangesAsync();
                return Ok();
            }
            else
            {
                return BadRequest("File null");
            }
        }
    }
}
