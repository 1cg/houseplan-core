﻿using housePLAN.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace housePLAN.Controllers
{
    public class DownloadPlans : Controller
    {
        private readonly ApplicationDbContext _context;

        public DownloadPlans(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult ListDocs(int id) // addressbaseid
        {
            var docs = _context.FileAttachments.Where(i => i.AddressBaseItemId == id).ToList();
            return PartialView(docs);
        }


        public IActionResult ViewDoc(int id, string password)
        {
            var doc = _context.FileAttachments.Where(i => i.Id == id).FirstOrDefault();

            var stream = new FileStream($"uploadeddocs/{doc.FileName}", FileMode.Open);

            if (stream == null)
                return NotFound(); // returns a NotFoundResult with Status404NotFound response.

            return File(stream, "application/octet-stream", doc.FileName); // returns a FileStreamResult
        }
    }
}
