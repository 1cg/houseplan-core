﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace housePLAN.Models
{
    public class FileAttachment : DbBase
    {
        public int AddressBaseItemId { get; set; }

        public string Description { get; set; }
        public string Guid { get; set; }
        public DateTime? UploadedOn { get; set; }
        public string FileName { get; set; }

        public string Password { get; set; }
        public bool Uploaded { get; set; }
    }
}
