﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace housePLAN.Models.VM
{
    public class SearchPropertyVM
    {
        public string Postcode { get; set; }
        public string Street { get; set; }
    }
}
