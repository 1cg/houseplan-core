﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace housePLAN.Models.VM
{
    public class PropertyAddressVM
    {
        public AddressBaseItem Address { get; set; }
        public string FullAddressPretty
        {
            get
            {
                return $"{Address.BUILDING_NUMBER} {Address.DEPENDENT_THOROUGHFARE}, {Address.DEPENDENT_LOCALITY}, {Address.POST_TOWN}, {Address.POSTCODE}";
            }
        }

        public bool HasFiles { get; set; }

    }
}
