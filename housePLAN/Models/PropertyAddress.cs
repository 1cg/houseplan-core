﻿using housePLAN.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace housePLAN.Models
{
    public class PropertyAddress : DbBase
    {
        [Display(Name = "House Name or Number")]
        public string NameOrNumber { get; set; }
        public string Street { get; set; }

        [Display(Name = "Town or City")]
        public string TownOrCity { get; set; }
        public string District { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        [Display(Name = "Property Type")]
        public PropertyType PropertyType { get; set; }

        [Display(Name = "Is Propery a New Build?")]
        public bool NewBuild { get; set; }

        [Display(Name = "Is Propery Freehold?")]
        public bool IsFreehold { get; set; }
        public DateTime? BuildDate { get; set; }

        public string FullAddressPretty
        {
            get
            {
                return $"{NameOrNumber} {Street}, {TownOrCity}, {Postcode}";
            }
        }
    }
}
