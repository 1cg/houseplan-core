﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace housePLAN.Models
{
    public class AddressBaseItem 
    {
        public int Id { get; set; }
        public double UPRN { get; set; }
        public string ORGANISATION_NAME { get; set; }

        [Display(Name = "Building Number")]
        public int? BUILDING_NUMBER { get; set; }
        [Display(Name = "Building Name")]
        public string BUILDING_NAME { get; set; }
        [Display(Name = "Sub Building Name")]
        public string SUB_BUILDING_NAME { get; set; }
        [Display(Name = "Street")]
        public string DEPENDENT_THOROUGHFARE { get; set; }
        [Display(Name = "Street")]
        public string THOROUGHFARE { get; set; }
        [Display(Name = "Town")]
        public string POST_TOWN { get; set; }
        [Display(Name = "Locality")]
        public string DEPENDENT_LOCALITY { get; set; }
        [Display(Name = "Postcode")]
        public string POSTCODE { get; set; }
        public decimal X_COORDINATE { get; set; }
        public decimal Y_COORDINATE { get; set; }
        public decimal LATITUDE { get; set; }
        public decimal LONGITUDE { get; set; }

        public string PostcodeTrimmed { get; set; }


        public virtual List<FileAttachment> FileAttachments { get; set; }
        public string FullAddressPretty
        {
            get
            {
                return $"{BUILDING_NUMBER} {DEPENDENT_THOROUGHFARE}, {DEPENDENT_LOCALITY}, {POST_TOWN}, {POSTCODE}";
            }
        }

    }
}
